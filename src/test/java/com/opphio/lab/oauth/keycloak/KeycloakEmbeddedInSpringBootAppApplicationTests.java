package com.opphio.lab.oauth.keycloak;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class KeycloakEmbeddedInSpringBootAppApplicationTests {

    @Test
    void contextLoads() {
    }

}
